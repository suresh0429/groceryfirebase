package com.suresh.firebasegrocery;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {
    public static String USER = "Users";
    public static String ADMIN = "Admin";

    @BindView(R.id.btnUser)
    Button btnUser;
    @BindView(R.id.btnAdmin)
    Button btnAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btnUser, R.id.btnAdmin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnUser:
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.putExtra("ROLE",USER);
                startActivity(intent);
                break;
            case R.id.btnAdmin:
                Intent intent1 = new Intent(MainActivity.this, LoginActivity.class);
                intent1.putExtra("ROLE",ADMIN);
                startActivity(intent1);
                break;
        }
    }
}
