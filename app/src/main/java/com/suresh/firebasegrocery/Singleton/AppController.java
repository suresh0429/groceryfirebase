package com.suresh.firebasegrocery.Singleton;

import android.app.Application;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.suresh.firebasegrocery.Storage.PrefManagerUser;
import com.suresh.firebasegrocery.User.Adapters.CartAdapter;
import com.suresh.firebasegrocery.User.CartActivity;
import com.suresh.firebasegrocery.User.Model.Product;

import java.util.ArrayList;

import static com.suresh.firebasegrocery.Constants.DATABASE_PATH_CART;
import static com.suresh.firebasegrocery.Constants.USERDATA_PATH;

public class AppController extends Application {
    public static final String TAG = AppController.class
            .getSimpleName();


    private static AppController mInstance;

    private PrefManagerUser pref;
    private static final String PREFS_NAME = "CARTCOUNT";

    ArrayList<Product> productArrayList;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        if (!FirebaseApp.getApps(this).isEmpty()) {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        }


    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }


    public void cartCount(final String userId) {


        productArrayList = new ArrayList<>();

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference(USERDATA_PATH).child(userId).child(DATABASE_PATH_CART);
        mDatabase.keepSynced(true);

        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {


                Log.e("SNAPSHOT", "" + snapshot.getValue());

                if (snapshot.getValue() != null) {

                    //iterating through all the values in database
                    productArrayList.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        Product upload = postSnapshot.getValue(Product.class);
                        upload.setmKey(postSnapshot.getKey());
                        productArrayList.add(upload);

                    }

                    int cartindex = productArrayList.size();

                    if (cartindex > 0) {
                        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, 0);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putInt("itemCount", cartindex);
                        editor.apply();

                    } else {
                        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, 0);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putInt("itemCount", 0);
                        editor.apply();
                    }


                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
