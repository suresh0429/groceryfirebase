package com.suresh.firebasegrocery;

public class Constants {
    //Userdata
    public static final String USERDATA_PATH = "UserData";

    //Banner
    public static final String STORAGE_PATH_BANNERS = "banners/";
    public static final String DATABASE_PATH_BANNERS = "banners";


    // Category
    public static final String STORAGE_PATH_CATEGORY = "category/";
    public static final String DATABASE_PATH_CATEGORY = "category";

    // Subcategory
    public static final String STORAGE_PATH_SUBCATEGORY = "subcategory/";
    public static final String DATABASE_PATH_SUBCATEGORY = "subcategory";

    // products
    public static final String STORAGE_PATH_PRODUCT = "products/";
    public static final String DATABASE_PATH_PRODUCTS = "products";

    // cart
    public static final String DATABASE_PATH_CART = "cartList";

    // wishlist
    public static final String DATABASE_PATH_WISHLIST = "wishlist";


    public static final String CART_WISH_STATUS = "StatusCartWish";
}
