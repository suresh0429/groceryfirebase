package com.suresh.firebasegrocery.Interface;

import com.suresh.firebasegrocery.User.Model.Product;

public interface Cartinterface {

    void onMinusClick(Product product);

    void onPlusClick(Product product);

    void onRemoveDialog(Product product);

    void onWishListDialog(Product product);

}
