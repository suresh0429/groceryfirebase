package com.suresh.firebasegrocery.Interface;

public interface OnRecyclerViewItemClickListener {

    public void onRecyclerViewItemClicked(int position, int id);

}
