package com.suresh.firebasegrocery.Model;

public class LoginResponse {


    /**
     * email : ksuresh.unique@gmail.com
     * mobile : 8985018103
     * password : suresh
     * role : Users
     * uId : fn13X0pydmbaqFqBO9oq6flMW0B3
     * username : suresh
     */

    private String email;
    private String mobile;
    private String password;
    private String role;
    private String uId;
    private String username;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUId() {
        return uId;
    }

    public void setUId(String uId) {
        this.uId = uId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
