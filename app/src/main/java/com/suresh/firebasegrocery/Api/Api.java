package com.suresh.firebasegrocery.Api;

import com.suresh.firebasegrocery.Model.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface Api {


    @GET
    Call<LoginResponse> userLogin(@Url String url);

}
