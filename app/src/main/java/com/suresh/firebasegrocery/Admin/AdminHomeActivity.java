package com.suresh.firebasegrocery.Admin;

import android.app.Activity;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.suresh.firebasegrocery.Admin.Adapter.AdminHomeAdapter;
import com.suresh.firebasegrocery.Admin.Model.HomeModel;
import com.suresh.firebasegrocery.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdminHomeActivity extends Activity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private List<HomeModel> homeModelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_home);
        ButterKnife.bind(this);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        AdminHomeAdapter adapter = new AdminHomeAdapter(this,homeModelList);
        recyclerView.setAdapter(adapter);

        adminModuleData();

    }

    private void adminModuleData(){
        HomeModel homeModel = new HomeModel("1","Add Home Banners","#FFC2185B");
        homeModelList.add(homeModel);

        homeModel = new HomeModel("2","Add category","#FF7B1FA2");
        homeModelList.add(homeModel);

        homeModel = new HomeModel("3","Add Subcategory","#FF512DA8");
        homeModelList.add(homeModel);

        homeModel = new HomeModel("4","Add Product","#FF303F9F");
        homeModelList.add(homeModel);

    }
}
