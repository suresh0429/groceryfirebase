package com.suresh.firebasegrocery.Admin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.suresh.firebasegrocery.Admin.BannersActivity;
import com.suresh.firebasegrocery.Admin.CategoryActivity;
import com.suresh.firebasegrocery.Admin.Model.HomeModel;
import com.suresh.firebasegrocery.LoginActivity;
import com.suresh.firebasegrocery.R;
import com.suresh.firebasegrocery.User.HomeActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdminHomeAdapter extends RecyclerView.Adapter<AdminHomeAdapter.MyViewHolder> {


    private Context mContext;
    private List<HomeModel> homeList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.cardview)
        CardView cardview;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);

        }
    }

    public AdminHomeAdapter(Context mContext, List<HomeModel> homekitchenList) {
        this.mContext = mContext;
        this.homeList = homekitchenList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final HomeModel home = homeList.get(position);

        holder.txtTitle.setText(home.getCatgoeryName());
        holder.cardview.setCardBackgroundColor(Color.parseColor(home.getColor()));

        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position==0){
                    mContext.startActivity(new Intent(mContext, BannersActivity.class));

                }else if (position==1){
                    mContext.startActivity(new Intent(mContext, CategoryActivity.class));

                }else if (position==2){
                   // mContext.startActivity(new Intent(mContext, HomeActivity.class));
                }else if (position==3){
                   // mContext.startActivity(new Intent(mContext, HomeActivity.class));
                }
            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }


}
