package com.suresh.firebasegrocery.Admin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.suresh.firebasegrocery.Admin.Model.ProductsModel;
import com.suresh.firebasegrocery.Admin.ProductActivity;
import com.suresh.firebasegrocery.Constants;
import com.suresh.firebasegrocery.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.MyViewHolder> {


    private Context mContext;
    private List<ProductsModel> productsModelList;
    private String catMKey,subCatMKey;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.thumbnail)
        ImageView thumbnail;
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtPrice)
        TextView txtPrice;
        @BindView(R.id.linear)
        LinearLayout linear;
        @BindView(R.id.delete)
        ImageView delete;
        @BindView(R.id.cardview)
        CardView cardview;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    public ProductsAdapter(Context mContext, List<ProductsModel> productsModelList, String catMKey, String subCatMKey) {
        this.mContext = mContext;
        this.productsModelList = productsModelList;
        this.catMKey = catMKey;
        this.subCatMKey = subCatMKey;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ProductsModel productsModel = productsModelList.get(position);

        // loading album cover using Glide library
        Glide.with(mContext).load(productsModel.getImageUrl()).into(holder.thumbnail);
        Log.e(TAG, "onBindViewHolder: " + productsModel.getImageUrl());

        holder.txtTitle.setText(productsModel.getProductName());
        holder.txtPrice.setText(mContext.getResources().getString(R.string.Rs)+productsModel.getProductPrice()+" ("+productsModel.getCapacityWeight()+")");

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //creating a popup menu
                PopupMenu popup = new PopupMenu(mContext, holder.delete);
                //inflating menu from xml resource
                popup.inflate(R.menu.options_menu);
                //adding click listener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.menu1) {//handle menu1 click
                            deleteArtist(productsModel.getmKey());
                            deleteImageFromStorage(productsModel.getmKey(), productsModel.getImageUrl());
                            productsModelList.remove(position);
                            notifyDataSetChanged();
                                /* case R.id.menu2:
                                //handle menu2 click
                                break;
                            case R.id.menu3:
                                //handle menu3 click
                                break;*/
                        }
                        return false;
                    }
                });
                //displaying the popup
                popup.show();

            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return productsModelList.size();
    }


    private boolean deleteArtist(String id) {
        //getting the specified artist reference
       // DatabaseReference dR = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_PRODUCTS).child(id);

        DatabaseReference dR = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_CATEGORY).child(catMKey).child(Constants.DATABASE_PATH_SUBCATEGORY).child(subCatMKey).child(Constants.STORAGE_PATH_PRODUCT).child(id);

        //removing artist
        dR.removeValue();



        Toast.makeText(mContext, "Post Deleted", Toast.LENGTH_LONG).show();

        return true;
    }

    private boolean deleteImageFromStorage(final String key, String imageUrl) {

        FirebaseStorage mStorage = FirebaseStorage.getInstance();
        final DatabaseReference mDatabaseRef = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_PRODUCTS);

        StorageReference imageRef = mStorage.getReferenceFromUrl(imageUrl);
        imageRef.delete().addOnSuccessListener(aVoid -> {
            mDatabaseRef.child(key).removeValue();
            Toast.makeText(mContext, "Item deleted", Toast.LENGTH_SHORT).show();
        });
        return true;
    }
}
