package com.suresh.firebasegrocery;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.suresh.firebasegrocery.Model.User;

import am.appwise.components.ni.NoInternetDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.suresh.firebasegrocery.Constants.USERDATA_PATH;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.textView2)
    TextView textView2;
    @BindView(R.id.etEmail)
    TextInputEditText etEmail;
    @BindView(R.id.etPassword)
    TextInputEditText etPassword;
    @BindView(R.id.btnRegister)
    Button btnRegister;
    @BindView(R.id.txtSignin)
    TextView txtSignin;
    @BindView(R.id.etUserName)
    TextInputEditText etUserName;
    @BindView(R.id.etPhone)
    TextInputEditText etPhone;

    //defining firebaseauth object
    private FirebaseAuth firebaseAuth;

    private ProgressDialog progressBar;

    private NoInternetDialog noInternetDialog;

    String ROLE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Register");

        if (getIntent() != null) {

            ROLE = getIntent().getStringExtra("ROLE");

        }

        noInternetDialog = new NoInternetDialog.Builder(this).build();

        //initializing firebase auth object
        firebaseAuth = FirebaseAuth.getInstance();


       /* //if getCurrentUser does not returns null
        if(firebaseAuth.getCurrentUser() != null){
            //that means user is already logged in
            //so close this activity
            finish();

            //and open profile activity
            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
        }*/


        progressBar = new ProgressDialog(this);

    }

    @OnClick({R.id.btnRegister, R.id.txtSignin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnRegister:
                registerUser();
                break;
            case R.id.txtSignin:
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                intent.putExtra("ROLE",ROLE);
                startActivity(intent);
                break;
        }
    }


    private void registerUser() {

        //getting email and password from edit texts
        String userName = etUserName.getText().toString().trim();
        String mobile = etPhone.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();


        if (userName.isEmpty()) {
            etUserName.setError(getString(R.string.input_error_name));
            etUserName.requestFocus();
            return;
        }

        if (email.isEmpty()) {
            etEmail.setError(getString(R.string.input_error_email));
            etEmail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.setError(getString(R.string.input_error_email_invalid));
            etEmail.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            etPassword.setError(getString(R.string.input_error_password));
            etPassword.requestFocus();
            return;
        }

        if (password.length() < 6) {
            etPassword.setError(getString(R.string.input_error_password_length));
            etPassword.requestFocus();
            return;
        }

        if (mobile.isEmpty()) {
            etPhone.setError(getString(R.string.input_error_phone));
            etPhone.requestFocus();
            return;
        }

        if (mobile.length() != 10) {
            etPhone.setError(getString(R.string.input_error_phone_invalid));
            etPhone.requestFocus();
            return;
        }



        //if the email and password are not empty
        //displaying a progress dialog

        progressBar.setMessage("Registering Please Wait...");
        progressBar.show();

        //creating a new user
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    //checking if success
                    if (task.isSuccessful()) {

                        User user = new User(
                                FirebaseAuth.getInstance().getCurrentUser().getUid(),
                                userName,
                                mobile,
                                email,
                                password,
                                ROLE
                        );

                        FirebaseDatabase.getInstance().getReference(USERDATA_PATH)
                                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                .setValue(user).addOnCompleteListener(task1 -> {
                            progressBar.dismiss();
                            if (task1.isSuccessful()) {

                                Toast.makeText(RegisterActivity.this, "registration success", Toast.LENGTH_LONG).show();
                                Intent intentAdmin = new Intent(getApplicationContext(), LoginActivity.class);
                                intentAdmin.putExtra("ROLE",ROLE);
                                startActivity(intentAdmin);

                            } else {
                                //display a failure message
                                Toast.makeText(RegisterActivity.this, task1.getException().getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });


                    } else {
                        //display some message here
                        Toast.makeText(RegisterActivity.this, "Registration Error", Toast.LENGTH_LONG).show();
                    }

                });

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                Intent intentAdmin = new Intent(getApplicationContext(), LoginActivity.class);
                intentAdmin.putExtra("ROLE",ROLE);
                startActivity(intentAdmin);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
