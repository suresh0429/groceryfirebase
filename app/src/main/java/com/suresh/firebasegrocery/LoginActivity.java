package com.suresh.firebasegrocery;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.suresh.firebasegrocery.Admin.AdminHomeActivity;
import com.suresh.firebasegrocery.Api.RetrofitClient;
import com.suresh.firebasegrocery.Model.LoginResponse;
import com.suresh.firebasegrocery.Storage.PrefManagerUser;
import com.suresh.firebasegrocery.Storage.PrefManagerAdmin;
import com.suresh.firebasegrocery.User.HomeActivity;

import am.appwise.components.ni.NoInternetDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class LoginActivity extends Activity {
    PrefManagerUser session;
    PrefManagerAdmin session1;
    public static String USER = "Users";
    public static String ADMIN = "Admin";

    public static String ROLE;

    @BindView(R.id.textView2)
    TextView textView2;
    @BindView(R.id.etEmail)
    TextInputEditText etEmail;
    @BindView(R.id.etPassword)
    TextInputEditText etPassword;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.txtSignup)
    TextView txtSignup;
    @BindView(R.id.txtAdmin)
    TextView txtAdmin;


    private FirebaseAuth firebaseAuth;

    //progress dialog
    private ProgressDialog progressDialog;

    private NoInternetDialog noInternetDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        session = new PrefManagerUser(this);
        session1 = new PrefManagerAdmin(this);


        if (getIntent() != null) {

            ROLE = getIntent().getStringExtra("ROLE");

        }

        noInternetDialog = new NoInternetDialog.Builder(this).build();

        //getting firebase auth object
        firebaseAuth = FirebaseAuth.getInstance();

        progressDialog = new ProgressDialog(this);


        if (ROLE.equalsIgnoreCase("Users")) {
            if (session.isLoggedIn()) {
                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                finish();
            }

        } else if (ROLE.equalsIgnoreCase("Admin")) {
            if (session1.isLoggedIn()) {
                startActivity(new Intent(LoginActivity.this, AdminHomeActivity.class));
                finish();
            }

        }


    }


    @OnClick({R.id.btnLogin, R.id.txtSignup, R.id.txtAdmin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:

                userLogin();
                break;
            case R.id.txtSignup:
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                intent.putExtra("ROLE", USER);
                startActivity(intent);
                break;

            case R.id.txtAdmin:
                Intent intentAdmin = new Intent(LoginActivity.this, RegisterActivity.class);
                intentAdmin.putExtra("ROLE", ADMIN);
                startActivity(intentAdmin);
                break;


        }
    }


    //method for user login
    private void userLogin() {

        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();


        //checking if email and passwords are empty
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Please enter email", Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_LONG).show();
            return;
        }

        //if the email and password are not empty
        //displaying a progress dialog

        progressDialog.setMessage("Login Please Wait...");
        progressDialog.show();

        //logging in the user
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    progressDialog.dismiss();
                    //if the task is successfull
                    if (task.isSuccessful()) {

                        Log.e(TAG, "userLogin: " + task.getResult().getUser().getUid());
                        customLogin(email, password, task.getResult().getUser().getUid());

                    } else {
                        Toast.makeText(LoginActivity.this, "Invalid Credentials", Toast.LENGTH_SHORT).show();
                    }
                });


    }

    private void customLogin(String email, String password, String uid) {
        Call<LoginResponse> call = RetrofitClient.getInstance().getApi().userLogin(RetrofitClient.BASE_URL + "UserData/" + uid + ".json");

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                progressDialog.dismiss();
                //  btnLogin.setEnabled(true);

                LoginResponse loginResponse = response.body();
                Log.d(TAG, "onResponse: " + loginResponse + "-----" + RetrofitClient.BASE_URL + "UserData/" + uid + ".json");

                if (loginResponse != null && loginResponse.getRole().equals("Admin")) {

                    session1.createLogin(loginResponse.getUId(),
                            loginResponse.getUsername(),
                            loginResponse.getMobile(),
                            loginResponse.getEmail(),
                            loginResponse.getRole());


                    Intent intent = new Intent(LoginActivity.this, AdminHomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);


                } else if (loginResponse != null && loginResponse.getRole().equals("Users")) {

                    session.createLogin(loginResponse.getUId(),
                            loginResponse.getUsername(),
                            loginResponse.getMobile(),
                            loginResponse.getEmail(),
                            loginResponse.getRole());


                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                } else {
                    int color = Color.RED;
                    // snackBar(loginResponse.getMessage(), color);
                    Toast.makeText(LoginActivity.this, response.message(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressDialog.dismiss();
                btnLogin.setEnabled(true);
                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }


}
