package com.suresh.firebasegrocery.User;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.suresh.firebasegrocery.Admin.Model.ProductsModel;
import com.suresh.firebasegrocery.Admin.Model.SubCategoryModel;
import com.suresh.firebasegrocery.Constants;
import com.suresh.firebasegrocery.R;
import com.suresh.firebasegrocery.Singleton.AppController;
import com.suresh.firebasegrocery.Storage.PrefManagerUser;
import com.suresh.firebasegrocery.User.Adapters.ProductAdapter;
import com.suresh.firebasegrocery.User.Adapters.SubCatRecyclerViewDataAdapter;
import com.suresh.firebasegrocery.User.Adapters.SubCatageoryAdapter;
import com.suresh.firebasegrocery.User.Model.HeaderSectionDataModel;
import com.suresh.firebasegrocery.User.Model.SingleItemModel;
import com.suresh.firebasegrocery.Utilities.Converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductListActivity extends AppCompatActivity {

    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.simpleSwipeRefreshLayout)
    SwipeRefreshLayout simpleSwipeRefreshLayout;

    //database reference
    private DatabaseReference mDatabase;

    private List<ProductsModel> productsModelList;

    String CatMKey, subcatMkey;

    String userId;
    int cartindex;

    private PrefManagerUser pref;
    AppController appController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        ButterKnife.bind(this);

        pref = new PrefManagerUser(this);
        appController = (AppController) getApplication();

        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        String username = profile.get("name");
        String email = profile.get("email");


        if (getIntent() != null) {
            CatMKey = getIntent().getStringExtra("CatMKey");
            subcatMkey = getIntent().getStringExtra("subcatMkey");
        }


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Products");

        simpleSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorBlue, R.color.colorPrimary);

        simpleSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                productsData(CatMKey, subcatMkey);

                simpleSwipeRefreshLayout.setRefreshing(false);


            }
        });

        productsData(CatMKey, subcatMkey);


    }


    private void productsData(String catMKey, String subcatMkey) {

        simpleSwipeRefreshLayout.setRefreshing(true);
        productsModelList = new ArrayList<>();
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_CATEGORY).child(CatMKey).child(Constants.DATABASE_PATH_SUBCATEGORY).child(subcatMkey).child(Constants.DATABASE_PATH_PRODUCTS);

        mDatabase.keepSynced(true);

        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog
                simpleSwipeRefreshLayout.setRefreshing(false);

                Log.e("SNAPSHOT", "" + snapshot.getValue());

                if (snapshot.getValue() != null) {

                    //iterating through all the values in database
                    productsModelList.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        ProductsModel upload = postSnapshot.getValue(ProductsModel.class);
                        upload.setmKey(postSnapshot.getKey());
                        productsModelList.add(upload);

                    }


                    // product Recycler
                    RecyclerView.LayoutManager mLayoutManager1 = new GridLayoutManager(getApplicationContext(), 2);
                    myRecyclerView.setLayoutManager(mLayoutManager1);
                    myRecyclerView.setItemAnimator(new DefaultItemAnimator());
                    myRecyclerView.setHasFixedSize(true);
                    myRecyclerView.setNestedScrollingEnabled(false);

                    // product Adapter
                    ProductAdapter productAdapter = new ProductAdapter(getApplicationContext(), productsModelList,catMKey,subcatMkey);
                    myRecyclerView.setAdapter(productAdapter);
                    productAdapter.notifyDataSetChanged();
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                simpleSwipeRefreshLayout.setRefreshing(false);
            }
        });


    }


    @Override
    protected void onRestart() {

        appController.cartCount(userId);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);
        invalidateOptionsMenu();
        super.onRestart();
    }

    @Override
    protected void onStart() {
        appController.cartCount(userId);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);
        invalidateOptionsMenu();
        super.onStart();
    }

    ///
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.actionbar_menu, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_cart);
         menuItem.setIcon(Converter.convertLayoutToImage(ProductListActivity.this, cartindex, R.drawable.ic_actionbar_bag));
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.action_cart:
                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case R.id.action_search:
                Intent intent1 = new Intent(ProductListActivity.this, SearchActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
