package com.suresh.firebasegrocery.User;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.suresh.firebasegrocery.Admin.Model.ProductsModel;
import com.suresh.firebasegrocery.Constants;
import com.suresh.firebasegrocery.R;
import com.suresh.firebasegrocery.Singleton.AppController;
import com.suresh.firebasegrocery.Storage.PrefManagerUser;
import com.suresh.firebasegrocery.User.Model.CartorWistStatus;
import com.suresh.firebasegrocery.User.Model.Product;
import com.suresh.firebasegrocery.Utilities.Converter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.views.BannerSlider;

import static com.suresh.firebasegrocery.Constants.CART_WISH_STATUS;
import static com.suresh.firebasegrocery.Constants.DATABASE_PATH_CART;
import static com.suresh.firebasegrocery.Constants.USERDATA_PATH;

public class ProductDetailsActivity extends AppCompatActivity {
    //firebase objects
    private DatabaseReference mDatabase;
    private String formattedDate;
    StorageReference sRef;
    private String TAG = "ProductDetailsActivity";

    @BindView(R.id.banner_slider1)
    BannerSlider bannerSlider1;
    @BindView(R.id.txtProductName)
    TextView txtProductName;
    @BindView(R.id.txtPrice)
    TextView txtPrice;
    @BindView(R.id.txtCapacity)
    TextView txtCapacity;
    @BindView(R.id.txtDisPrice)
    TextView txtDisPrice;
    @BindView(R.id.txtDescription)
    TextView txtDescription;
    @BindView(R.id.btnwishlist)
    Button btnwishlist;
    @BindView(R.id.btnAddtoCart)
    Button btnAddtoCart;
    @BindView(R.id.bottomlayout)
    CardView bottomlayout;
    @BindView(R.id.progressBar2)
    ProgressBar progressBar2;

    private FirebaseStorage mStorage;

    //list to hold all the uploaded images
    private List<ProductsModel> productsModelList;
    private List<CartorWistStatus> cartorWistStatusList;

    String catMKey, subCatMKey, productName, mKey, userId, id, currentDate, imageType, imageUrl,
            category, subcategory, productPrice, capacityWeight, availability, description, wishlistStatus, cartListStatus;
    private PrefManagerUser pref;
    int cartindex;
    AppController appController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);

        appController = (AppController) getApplication();
        pref = new PrefManagerUser(getApplicationContext());
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");

        if (getIntent() != null) {
            catMKey = getIntent().getStringExtra("CatMKey");
            subCatMKey = getIntent().getStringExtra("subcatMkey");
            productName = getIntent().getStringExtra("productName");
            mKey = getIntent().getStringExtra("mKey");
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(productName);

        productsData(catMKey, subCatMKey, mKey);

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedDate = df.format(c.getTime());


    }

    private void productsData(String catMKey, String subCatmKey, String mKey) {

        progressBar2.setVisibility(View.VISIBLE);
        productsModelList = new ArrayList<>();
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_CATEGORY).child(catMKey).child(Constants.DATABASE_PATH_SUBCATEGORY).child(subCatmKey).child(Constants.DATABASE_PATH_PRODUCTS);
        // mDatabase.keepSynced(true);

        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog

                progressBar2.setVisibility(View.GONE);
                Log.e("SNAPSHOT", "" + snapshot.getValue());

                if (snapshot.getValue() != null) {

                    //iterating through all the values in database
                    productsModelList.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        ProductsModel upload = postSnapshot.getValue(ProductsModel.class);
                        upload.setmKey(postSnapshot.getKey());
                        productsModelList.add(upload);

                    }

                    for (ProductsModel productsModel : productsModelList) {

                        if (productsModel.getmKey().equalsIgnoreCase(mKey)) {

                            id = productsModel.getId();
                            currentDate = productsModel.getCurrentDate();
                            imageType = productsModel.getImageType();
                            imageUrl = productsModel.getImageUrl();
                            category = productsModel.getCategory();
                            subcategory = productsModel.getSubcategory();
                            productPrice = productsModel.getProductPrice();
                            capacityWeight = productsModel.getCapacityWeight();
                            availability = productsModel.getAvailability();
                            description = productsModel.getDescription();

                            // home banners
                            List<Banner> banners = new ArrayList<>();
                            BannerSlider bannerSlider = (BannerSlider) findViewById(R.id.banner_slider1);
                            banners.add(new RemoteBanner(productsModel.imageUrl));

                            //add banner using resource drawable
                            bannerSlider.setBanners(banners);
                            bannerSlider.onIndicatorSizeChange();

                            txtProductName.setText(productsModel.productName);
                            txtPrice.setText(getResources().getString(R.string.Rs) + productsModel.productPrice);
                            txtCapacity.setText(productsModel.capacityWeight);
                            txtDescription.setText(productsModel.description);


                        }

                    }

                    status(catMKey, subCatMKey, mKey, id);

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressBar2.setVisibility(View.GONE);


            }
        });


    }

    private void status(String catMKey, String subCatmKey, String mKey, String id) {

        progressBar2.setVisibility(View.VISIBLE);
        cartorWistStatusList = new ArrayList<>();

        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_CATEGORY).child(catMKey).child(Constants.DATABASE_PATH_SUBCATEGORY).child(subCatmKey).child(Constants.DATABASE_PATH_PRODUCTS).child(id).child(CART_WISH_STATUS);
        mDatabase.keepSynced(true);

        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog

                progressBar2.setVisibility(View.GONE);
                Log.e("STATUSNAPSHOT", "" + snapshot.getValue());

                if (snapshot.getValue() != null) {

                    //iterating through all the values in database
                    cartorWistStatusList.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        CartorWistStatus upload = postSnapshot.getValue(CartorWistStatus.class);
                        upload.setmKey(userId);
                        cartorWistStatusList.add(upload);

                    }


                    for (CartorWistStatus cartorWistStatus : cartorWistStatusList) {

                        if (cartorWistStatus.getUserId().equalsIgnoreCase(userId)) {

                            wishlistStatus = cartorWistStatus.getWishlistStatus();
                            cartListStatus = cartorWistStatus.getCartStatus();

                            // wishlist status
                            if (cartorWistStatus.getWishlistStatus().equalsIgnoreCase("1")) {
                                btnwishlist.setText("Go to Wishlist");
                            } else {
                                btnwishlist.setText("Add to Wishlist");

                            }


                            // cart status
                            if (cartorWistStatus.getCartStatus().equalsIgnoreCase("1")) {
                                btnAddtoCart.setText("Go to Cart");

                            } else {
                                btnAddtoCart.setText("Add to Cart");

                            }
                        }
                    }


                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressBar2.setVisibility(View.GONE);


            }
        });


    }


    @OnClick({R.id.btnwishlist, R.id.btnAddtoCart})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnwishlist:

                if (btnwishlist.getText().equals("Go to Wishlist")) {
                    Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    whishlist();
                }
                break;
            case R.id.btnAddtoCart:
                if (btnAddtoCart.getText().equals("Go to Cart")) {
                    Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    addtocart();
                }
                break;
        }
    }

    private void whishlist() {

        btnwishlist.setText("Go To Wishlist");

        Product product = new Product();
        //Adding values
        product.setUserId(userId);
        product.setId(id);
        product.setCurrentDate(currentDate);
        product.setImageType(imageType);
        product.setImageUrl(imageUrl);
        product.setCategory(category);
        product.setSubcategory(subcategory);
        product.setProductName(productName);
        product.setProductPrice(Double.parseDouble(productPrice));
        product.setCapacityWeight(capacityWeight);
        product.setAvailability(Integer.parseInt(availability));
        product.setDescription(description);
        product.setmKey(mKey);
        product.setQuantity(1);
        product.setCatMKey(catMKey);
        product.setSubCatMKey(subCatMKey);
        if (null != cartListStatus) {
            product.setCartStatus(cartListStatus);
        }else {
            product.setCartStatus("0");
        }
        product.setWishStatus("1");


        mDatabase = FirebaseDatabase.getInstance().getReference(USERDATA_PATH).child(userId).child(Constants.DATABASE_PATH_WISHLIST);
        // String uploadId = mDatabase.push().getKey();
        DatabaseReference newRef = mDatabase.push();
        newRef.setValue(product);


        if (null != cartListStatus) {
            updateCart(userId, cartListStatus, "1", id);
        } else {
            updateCart(userId, "0", "1", id);

        }


    }

    private void addtocart() {

        btnAddtoCart.setText("Go To Cart");

        Product product = new Product();
        //Adding values
        product.setUserId(userId);
        product.setId(id);
        product.setCurrentDate(currentDate);
        product.setImageType(imageType);
        product.setImageUrl(imageUrl);
        product.setCategory(category);
        product.setSubcategory(subcategory);
        product.setProductName(productName);
        product.setProductPrice(Double.parseDouble(productPrice));
        product.setCapacityWeight(capacityWeight);
        product.setAvailability(Integer.parseInt(availability));
        product.setDescription(description);
        product.setmKey(mKey);
        product.setQuantity(1);
        product.setCatMKey(catMKey);
        product.setSubCatMKey(subCatMKey);
        product.setCartStatus("1");

        if (null != wishlistStatus) {
            product.setWishStatus(wishlistStatus);
        }else {
            product.setWishStatus("0");

        }


        mDatabase = FirebaseDatabase.getInstance().getReference(USERDATA_PATH).child(userId).child(DATABASE_PATH_CART);
        // String uploadId = mDatabase.push().getKey();
        DatabaseReference newRef = mDatabase.push();
        newRef.setValue(product);


        if (null != cartListStatus) {
            updateCart(userId, "1", wishlistStatus, id);

        } else {
            updateCart(userId, "1", "0", id);

        }


    }

    private void updateCart(String userId, String cartStatus, String wishStatus, String id) {
        //getting the specified artist reference
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_CATEGORY).child(catMKey).child(Constants.DATABASE_PATH_SUBCATEGORY).child(subCatMKey).child(Constants.DATABASE_PATH_PRODUCTS).child(id).child(CART_WISH_STATUS);
        //updating artist
        CartorWistStatus product = new CartorWistStatus();
        //Adding values
        product.setUserId(userId);
        product.setCartStatus(cartStatus);
        product.setWishlistStatus(wishStatus);
       // CartorWistStatus product = new CartorWistStatus(userId, cartStatus, wishStatus);
        mDatabase.child(userId).setValue(product);

    }


    @Override
    protected void onRestart() {

        appController.cartCount(userId);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);
        invalidateOptionsMenu();
        super.onRestart();
    }

    @Override
    protected void onStart() {
        appController.cartCount(userId);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);
        invalidateOptionsMenu();
        super.onStart();
    }

    ///
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.actionbar_menu, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_cart);
         menuItem.setIcon(Converter.convertLayoutToImage(ProductDetailsActivity.this, cartindex, R.drawable.ic_actionbar_bag));
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.action_cart:
                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case R.id.action_search:
                Intent intent1 = new Intent(ProductDetailsActivity.this, SearchActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
