package com.suresh.firebasegrocery.User.Adapters;

/**
 * Created by pratap.kesaboyina on 24-12-2014.
 */

import android.content.Context;

import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.suresh.firebasegrocery.R;
import com.suresh.firebasegrocery.User.Model.HeaderSectionDataModel;
import com.suresh.firebasegrocery.User.Model.SingleItemModel;
import com.suresh.firebasegrocery.User.ProductListActivity;

import java.util.ArrayList;

public class SubCatRecyclerViewDataAdapter extends RecyclerView.Adapter<SubCatRecyclerViewDataAdapter.ItemRowHolder> {

    private ArrayList<HeaderSectionDataModel> dataList;
    private Context mContext;
    private String CatMKey;

    public SubCatRecyclerViewDataAdapter(Context context, ArrayList<HeaderSectionDataModel> dataList, String CatMKey) {
        this.dataList = dataList;
        this.mContext = context;
        this.CatMKey = CatMKey;
    }

    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, null);
        ItemRowHolder mh = new ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ItemRowHolder itemRowHolder, int i) {


        final String sectionName = dataList.get(i).getHeaderTitle();

        ArrayList singleSectionItems = dataList.get(i).getAllItemsInSection();

        itemRowHolder.itemTitle.setText(sectionName);

        ProductSectionListDataAdapter itemListDataAdapter = new ProductSectionListDataAdapter(mContext, singleSectionItems,CatMKey,dataList.get(i).getSubCatMkey());

        itemRowHolder.recycler_view_list.setHasFixedSize(true);
        itemRowHolder.recycler_view_list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        itemRowHolder.recycler_view_list.setAdapter(itemListDataAdapter);


        itemRowHolder.recycler_view_list.setNestedScrollingEnabled(false);

        itemRowHolder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, ProductListActivity.class);
                intent.putExtra("CatMKey", CatMKey);
                intent.putExtra("subcatMkey", dataList.get(i).getSubCatMkey());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });

        if(i %4 == 0)
        {
            itemRowHolder.cardView.setBackgroundResource(R.drawable.gradient_brokenhearts);
            //  holder.imageView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        else if (i %4 == 1)
        {
            itemRowHolder.cardView.setBackgroundResource(R.drawable.gradient_orange);
            //  holder.imageView.setBackgroundColor(Color.parseColor("#FFFAF8FD"));
        }else if (i %4 == 2){
            itemRowHolder.cardView.setBackgroundResource(R.drawable.gradient_green);


        }
        else if (i %4 == 3){
            itemRowHolder.cardView.setBackgroundResource(R.drawable.gradient_shifty);

        }



       /* Glide.with(mContext)
                .load(feedItem.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.drawable.bg)
                .into(feedListRowHolder.thumbView);*/
    }

    @Override
    public int getItemCount() {
        return (null != dataList ? dataList.size() : 0);
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView itemTitle;

        protected RecyclerView recycler_view_list;

        protected Button btnMore;
        protected CardView cardView;


        public ItemRowHolder(View view) {
            super(view);

            this.itemTitle = (TextView) view.findViewById(R.id.itemTitle);
            this.recycler_view_list = (RecyclerView) view.findViewById(R.id.recycler_view_list);
            this.btnMore = (Button) view.findViewById(R.id.btnMore);
            this.cardView = (CardView) view.findViewById(R.id.cardView);


        }

    }

}