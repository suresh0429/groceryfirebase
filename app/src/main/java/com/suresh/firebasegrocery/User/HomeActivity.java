package com.suresh.firebasegrocery.User;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.suresh.firebasegrocery.Admin.Adapter.CategoryAdapter;
import com.suresh.firebasegrocery.Admin.CategoryActivity;
import com.suresh.firebasegrocery.Admin.Model.BannerModel;
import com.suresh.firebasegrocery.Admin.Model.CategoryModel;
import com.suresh.firebasegrocery.Constants;
import com.suresh.firebasegrocery.MainActivity;
import com.suresh.firebasegrocery.R;
import com.suresh.firebasegrocery.Singleton.AppController;
import com.suresh.firebasegrocery.Storage.PrefManagerUser;
import com.suresh.firebasegrocery.User.Adapters.UserCatAdapter;
import com.suresh.firebasegrocery.Utilities.Converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import am.appwise.components.ni.NoInternetDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.views.BannerSlider;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.banner_slider1)
    BannerSlider bannerSlider1;
    @BindView(R.id.searchLayout)
    TextView searchLayout;
    @BindView(R.id.homeRecycler)
    RecyclerView homeRecycler;
    @BindView(R.id.snackBar)
    LinearLayout snackBar;
    @BindView(R.id.parentLayout)
    NestedScrollView parentLayout;
    @BindView(R.id.simpleSwipeRefreshLayout)
    SwipeRefreshLayout simpleSwipeRefreshLayout;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    private FirebaseAuth firebaseAuth;
    private boolean doubleBackToExitPressedOnce = false;
    private NoInternetDialog noInternetDialog;
    private PrefManagerUser pref;
    AppController appController;
    String title,userId;
    int cartindex;

    //database reference
    private DatabaseReference mDatabase;

    private FirebaseStorage mStorage;

    //list to hold all the uploaded images
    private List<BannerModel> uploads;
    private List<CategoryModel> categoryModelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mStorage = FirebaseStorage.getInstance();



        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        noInternetDialog = new NoInternetDialog.Builder(this).build();

        //getting firebase auth object
        firebaseAuth = FirebaseAuth.getInstance();
        pref = new PrefManagerUser(this);
        appController = (AppController) getApplication();

        simpleSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorBlue, R.color.colorPrimary);
        simpleSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                prepareBannersData();
                prepareHomeData();
                simpleSwipeRefreshLayout.setRefreshing(false);


            }
        });

        prepareBannersData();
        prepareHomeData();


        homeRecycler.setHasFixedSize(true);
        homeRecycler.setLayoutManager(new GridLayoutManager(HomeActivity.this, 2));
        homeRecycler.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        homeRecycler.setItemAnimator(new DefaultItemAnimator());



        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        String username = profile.get("name");
        String email = profile.get("email");

        cartCount();


    }


    private void prepareBannersData() {

        uploads = new ArrayList<>();

        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_BANNERS);
        mDatabase.keepSynced(true);
        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog

                Log.e("SNAPSHOT", "" + snapshot.getValue());

                if (snapshot.getValue() != null) {

                    //iterating through all the values in database
                    uploads.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        BannerModel upload = postSnapshot.getValue(BannerModel.class);
                        upload.setmKey(postSnapshot.getKey());
                        uploads.add(upload);

                    }
                    // home banners
                    List<Banner> banners = new ArrayList<>();
                    /*final List<BannerModel> homeBannersBeanList = uploads;*/
                    BannerSlider bannerSlider = (BannerSlider) findViewById(R.id.banner_slider1);

                    for (final BannerModel homeBannersBean : uploads) {

                        // viewPagerItemslist.add(new ViewPagerItem(homeBannersBean.getImage()));
                        banners.add(new RemoteBanner(homeBannersBean.getImageUrl()));

                    }
                    //add banner using resource drawable
                    bannerSlider.setBanners(banners);
                    bannerSlider.onIndicatorSizeChange();


                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void prepareHomeData() {

        simpleSwipeRefreshLayout.setRefreshing(true);

        categoryModelList = new ArrayList<>();

        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_CATEGORY);
        mDatabase.keepSynced(true);

        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog
                simpleSwipeRefreshLayout.setRefreshing(false);

                Log.e("SNAPSHOT", "" + snapshot.getValue());

                if (snapshot.getValue() != null) {

                    //iterating through all the values in database
                    categoryModelList.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        CategoryModel upload = postSnapshot.getValue(CategoryModel.class);
                        upload.setmKey(postSnapshot.getKey());
                        categoryModelList.add(upload);

                    }
                    //creating adapter
                    UserCatAdapter adapter = new UserCatAdapter(getApplicationContext(), categoryModelList);
                    homeRecycler.setAdapter(adapter);

                    adapter.notifyDataSetChanged();


                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                simpleSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    // cart count
    private void cartCount() {

        appController.cartCount(userId);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindex", "" + cartindex);
        invalidateOptionsMenu();


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            /*super.onBackPressed();*/
            if (doubleBackToExitPressedOnce) {
                moveTaskToBack(true);
                Process.killProcess(Process.myPid());
                System.exit(1);
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }


    }


    @Override
    protected void onResume() {
        super.onResume();
        cartCount();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        cartCount();
    }

    @Override
    protected void onStart() {
        super.onStart();
        cartCount();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_cart);

        menuItem.setIcon(Converter.convertLayoutToImage(HomeActivity.this, cartindex, R.drawable.ic_actionbar_bag));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cart) {
            Intent intent = new Intent(getApplicationContext(), CartActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_account) {
            //My Account
           /* Intent intent = new Intent(HomeActivity.this, MyAccountActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);*/


        } else if (id == R.id.My_orders) {
            //My orders
           /* Intent intent = new Intent(HomeActivity.this, OrderIdActvity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);*/


        } else if (id == R.id.nav_logout) {
            pref.clearSession();
            signOut();

        } else if (id == R.id.nav_wishlist) {

           /* Intent intent = new Intent(getApplicationContext(), WishListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);*/

        } else if (id == R.id.nav_share) {
            //share app
            try {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, "PickAny24x7");
                String sAux = "\nInstall PickAny24x7 application click below link\n\n";
                sAux = sAux + "https://play.google.com/store/apps/details?id=com.prism.pickany247 \n\n";
                i.putExtra(Intent.EXTRA_TEXT, sAux);
                startActivity(Intent.createChooser(i, "choose one"));
            } catch (Exception e) {
                //e.toString();
            }

        } else if (id == R.id.nav_contactus) {

            title = "Contact Us";
           /* Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
            intent.putExtra("title", title);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);*/

        } else if (id == R.id.nav_support) {


           /* Intent intent = new Intent(getApplicationContext(), SupportUsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);*/

        } else if (id == R.id.nav_privacy) {

           /* title = "Privacy & Policy";
            Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
            intent.putExtra("title", title);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);*/
        } else if (id == R.id.nav_terms) {

           /* title = "Terms & Conditions";
            Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
            intent.putExtra("title", title);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);*/
        } else if (id == R.id.nav_return_policy) {

            // title = "Return Policy";
           /* Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
            intent.putExtra("title", title);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);*/
        } else if (id == R.id.nav_faq) {

            /*title = "FAQ's";
            Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
            intent.putExtra("title", title);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);*/
        } else if (id == R.id.nav_aboutus) {

            title = "About Us";
           /* Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
            intent.putExtra("title", title);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);*/
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }


    //sign out method
    public void signOut() {
        /*if (!pref.isLoggedIn()) {

            pref.clearSession();

        }*/
        pref.clearSession();
        firebaseAuth.signOut();
        Intent intent = new Intent(HomeActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

}
