package com.suresh.firebasegrocery.User.Adapters;

/**
 * Created by pratap.kesaboyina on 24-12-2014.
 */

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.suresh.firebasegrocery.R;
import com.suresh.firebasegrocery.User.Model.SingleItemModel;
import com.suresh.firebasegrocery.User.ProductDetailsActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductSectionListDataAdapter extends RecyclerView.Adapter<ProductSectionListDataAdapter.SingleItemRowHolder> {


    private ArrayList<SingleItemModel> itemsList;
    String subCatMkey;
    String catMkey;
    private Context mContext;

    public ProductSectionListDataAdapter(Context context, ArrayList<SingleItemModel> itemsList,  String catMkey,String subCatMkey) {
        this.itemsList = itemsList;
        this.mContext = context;
        this.subCatMkey = subCatMkey;
        this.catMkey = catMkey;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_single_card, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {

        SingleItemModel singleItem = itemsList.get(i);

        holder.txtproductName.setText(singleItem.getProductName());
        holder.txtproductPrice.setText("\u20B9"+singleItem.getProductPrice() +"("+singleItem.getCapacityWeight()+")");
       // holder.txtDiscountPrice.setText(singleItem.get());


        Glide.with(mContext)
                .load(singleItem.getImageUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                // .error(R.drawable.bg)
                .into(holder.itemImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, singleItem.getProductName(), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(mContext, ProductDetailsActivity.class);
                intent.putExtra("CatMKey", catMkey);
                intent.putExtra("subcatMkey", subCatMkey);
                intent.putExtra("productName", singleItem.getProductName());
                intent.putExtra("mKey", singleItem.getmKey());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.itemImage)
        ImageView itemImage;
        @BindView(R.id.txtproductName)
        TextView txtproductName;
        @BindView(R.id.txtproductPrice)
        TextView txtproductPrice;
        @BindView(R.id.txtDiscountPrice)
        TextView txtDiscountPrice;
        @BindView(R.id.txtDiscountTag)
        TextView txtDiscountTag;
        @BindView(R.id.tagLayout)
        LinearLayout tagLayout;

        public SingleItemRowHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);


           /* view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Toast.makeText(v.getContext(), tvTitle.getText(), Toast.LENGTH_SHORT).show();

                }
            });
*/

        }

    }

}