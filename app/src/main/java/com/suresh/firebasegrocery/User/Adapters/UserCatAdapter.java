package com.suresh.firebasegrocery.User.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.suresh.firebasegrocery.Admin.Adapter.CategoryAdapter;
import com.suresh.firebasegrocery.Admin.Model.CategoryModel;
import com.suresh.firebasegrocery.Admin.SubCategoryActivity;
import com.suresh.firebasegrocery.Constants;
import com.suresh.firebasegrocery.Interface.OnRecyclerViewItemClickListener;
import com.suresh.firebasegrocery.R;
import com.suresh.firebasegrocery.User.GroceryHomeActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class UserCatAdapter extends RecyclerView.Adapter<UserCatAdapter.MyViewHolder> {
    private Context mContext;
    private List<CategoryModel> categoryModels;
    // ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView thumbNail;
        TextView txtTitle, txtPending;
        ProgressBar progressBar;
        //  LinearLayout tagLayout;
        CardView cardView;


        public MyViewHolder(View view) {
            super(view);

            thumbNail = (ImageView) view.findViewById(R.id.thumbnail);
            txtTitle = (TextView) view.findViewById(R.id.txtTitle);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            // tagLayout = (LinearLayout) view.findViewById(R.id.tagLayout);
            // txtPending = (TextView) view.findViewById(R.id.txtPending);
            cardView = (CardView) view.findViewById(R.id.cardView);

        }
    }

    public UserCatAdapter(Context mContext, List<CategoryModel> categoryModels) {
        this.mContext = mContext;
        this.categoryModels = categoryModels;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_card1, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final CategoryModel home = categoryModels.get(position);

        holder.txtTitle.setText(home.getCategory());
        Glide.with(mContext).load(home.getImageUrl()).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.placeholder).into(holder.thumbNail);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mContext, GroceryHomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("MKey",home.getmKey());
                intent.putExtra("CatName",home.getCategory());
                mContext.startActivity(intent);

            }

        });


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return categoryModels.size();
    }


/*
    @Override
    public int getItemViewType(int position) {
        if(position% 2 == 1) {
            return 2;
        }else{
            return 3;
        }
    }
*/
}
