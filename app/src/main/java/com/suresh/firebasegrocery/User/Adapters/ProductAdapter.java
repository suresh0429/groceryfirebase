package com.suresh.firebasegrocery.User.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.suresh.firebasegrocery.Admin.Model.ProductsModel;
import com.suresh.firebasegrocery.R;
import com.suresh.firebasegrocery.User.ProductDetailsActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {
    private Context mContext;
    private List<ProductsModel> productsModelList;
    String catMKey,subcatMkey;
    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.thumbnail)
        ImageView thumbnail;
        @BindView(R.id.txtproductName)
        TextView txtproductName;
        @BindView(R.id.txtproductPrice)
        TextView txtproductPrice;
        @BindView(R.id.txtDiscountPrice)
        TextView txtDiscountPrice;
        @BindView(R.id.txtDiscountTag)
        TextView txtDiscountTag;
        @BindView(R.id.tagLayout)
        LinearLayout tagLayout;
        @BindView(R.id.parentLayout)
        CardView parentLayout;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    public ProductAdapter(Context mContext, List<ProductsModel> productsModelList, String catMKey, String subcatMkey) {
        this.mContext = mContext;
        this.productsModelList = productsModelList;
        this.catMKey = catMKey;
        this.subcatMkey = subcatMkey;

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_item_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ProductsModel productsModel = productsModelList.get(position);

        // loading album cover using Glide library
        Glide.with(mContext).load(productsModel.getImageUrl()).into(holder.thumbnail);
        Log.e(TAG, "onBindViewHolder: " + productsModel.getImageUrl());

        holder.txtproductName.setText(productsModel.getProductName());
        holder.txtproductPrice.setText(mContext.getResources().getString(R.string.Rs) + productsModel.getProductPrice() + " (" + productsModel.getCapacityWeight() + ")");

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ProductDetailsActivity.class);
                intent.putExtra("CatMKey", catMKey);
                intent.putExtra("subcatMkey", subcatMkey);
                intent.putExtra("productName", productsModel.productName);
                intent.putExtra("mKey", productsModel.getmKey());
                Log.d(TAG, "onClick: "+productsModel.getmKey());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);


            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return productsModelList.size();
    }


}
