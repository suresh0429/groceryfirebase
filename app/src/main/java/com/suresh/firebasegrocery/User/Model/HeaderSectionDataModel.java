package com.suresh.firebasegrocery.User.Model;

import java.util.ArrayList;

/**
 * Created by pratap.kesaboyina on 30-11-2015.
 */
public class HeaderSectionDataModel {



    private String headerTitle;
    private String subCatMkey;
    private ArrayList<SingleItemModel> allItemsInSection;


    public HeaderSectionDataModel() {

    }

    public HeaderSectionDataModel(String headerTitle, String subCatMkey, ArrayList<SingleItemModel> allItemsInSection) {
        this.headerTitle = headerTitle;
        this.subCatMkey = subCatMkey;
        this.allItemsInSection = allItemsInSection;
    }

    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }

    public String getSubCatMkey() {
        return subCatMkey;
    }

    public void setSubCatMkey(String subCatMkey) {
        this.subCatMkey = subCatMkey;
    }

    public ArrayList<SingleItemModel> getAllItemsInSection() {
        return allItemsInSection;
    }

    public void setAllItemsInSection(ArrayList<SingleItemModel> allItemsInSection) {
        this.allItemsInSection = allItemsInSection;
    }


}
