package com.suresh.firebasegrocery.User;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.suresh.firebasegrocery.Constants;
import com.suresh.firebasegrocery.Interface.Cartinterface;
import com.suresh.firebasegrocery.R;
import com.suresh.firebasegrocery.Storage.PrefManagerUser;
import com.suresh.firebasegrocery.User.Adapters.CartAdapter;
import com.suresh.firebasegrocery.User.Model.CartorWistStatus;
import com.suresh.firebasegrocery.User.Model.Product;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.suresh.firebasegrocery.Constants.CART_WISH_STATUS;
import static com.suresh.firebasegrocery.Constants.DATABASE_PATH_CART;
import static com.suresh.firebasegrocery.Constants.USERDATA_PATH;

public class CartActivity extends AppCompatActivity implements Cartinterface {
    @BindView(R.id.simpleSwipeRefreshLayout)
    SwipeRefreshLayout simpleSwipeRefreshLayout;
    private DatabaseReference mDatabase;
    @BindView(R.id.recyclerCart)
    RecyclerView recyclerCart;
    ArrayList<Product> productArrayList;

    Cartinterface cartinterface;
    CartAdapter cartAdapter;
    private PrefManagerUser pref;
    String userId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Cart");

        pref = new PrefManagerUser(getApplicationContext());
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");

        cartinterface = (Cartinterface) this;

        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerCart.setLayoutManager(mLayoutManager1);
        recyclerCart.setItemAnimator(new DefaultItemAnimator());

        simpleSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorBlue, R.color.colorPrimary);

        simpleSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                prepareCartData();

                simpleSwipeRefreshLayout.setRefreshing(false);


            }
        });



        prepareCartData();

    }


    private void prepareCartData() {

        simpleSwipeRefreshLayout.setRefreshing(true);

        productArrayList = new ArrayList<>();

        mDatabase = FirebaseDatabase.getInstance().getReference(USERDATA_PATH).child(userId).child(DATABASE_PATH_CART);
        mDatabase.keepSynced(true);

        //adding an event listener to fetch values
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                //dismissing the progress dialog
                simpleSwipeRefreshLayout.setRefreshing(false);

                Log.e("SNAPSHOT", "" + snapshot.getValue());

                if (snapshot.getValue() != null) {

                    //iterating through all the values in database
                    productArrayList.clear();
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        Product upload = postSnapshot.getValue(Product.class);
                        upload.setmKey(postSnapshot.getKey());
                        productArrayList.add(upload);

                    }
                    cartAdapter = new CartAdapter(CartActivity.this, productArrayList, cartinterface, "", "");
                    recyclerCart.setAdapter(cartAdapter);
                    cartAdapter.notifyDataSetChanged();


                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                simpleSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    @Override
    public void onMinusClick(Product product) {
        int i = productArrayList.indexOf(product);

        if (product.quantity > 1) {

            Product updatedProduct = new Product(
                    product.userId,
                    product.id,
                    product.currentDate,
                    product.imageType,
                    product.imageUrl,
                    product.category,
                    product.subcategory,
                    product.productName,
                    product.productPrice,
                    product.capacityWeight,
                    product.availability,
                    product.description,
                    product.mKey,
                    product.quantity - 1,
                    product.catMKey,
                    product.subCatMKey,
                    product.cartStatus,
                    product.wishStatus


            );
            productArrayList.remove(product);
            productArrayList.add(i, updatedProduct);
            cartAdapter.notifyDataSetChanged();


           /* updateQuantity(updatedProduct);
            calculateCartTotal(productArrayList);*/
        }
    }

    @Override
    public void onPlusClick(Product product) {
        int i = productArrayList.indexOf(product);

        Product updatedProduct = new Product(
                product.userId,
                product.id,
                product.currentDate,
                product.imageType,
                product.imageUrl,
                product.category,
                product.subcategory,
                product.productName,
                product.productPrice,
                product.capacityWeight,
                product.availability,
                product.description,
                product.mKey,
                product.quantity + 1,
                product.catMKey,
                product.subCatMKey,
                product.cartStatus,
                product.wishStatus

        );
        productArrayList.remove(product);
        productArrayList.add(i, updatedProduct);
        cartAdapter.notifyDataSetChanged();


           /* updateQuantity(updatedProduct);
            calculateCartTotal(productArrayList);*/

    }


    @Override
    public void onRemoveDialog(Product product) {

        deleteCartItem(product.getmKey(),product);
        updateCart(userId,product);


    }

    @Override
    public void onWishListDialog(Product product) {

    }


    private boolean deleteCartItem(String mKey, Product product) {
        //getting the specified artist reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference(USERDATA_PATH).child(userId).child(DATABASE_PATH_CART).child(mKey);
        //removing artist
        dR.removeValue();
        productArrayList.remove(product);
        cartAdapter.notifyDataSetChanged();

       // Toast.makeText(getApplicationContext(), "Post Deleted", Toast.LENGTH_LONG).show();

        return true;
    }

    private void updateCart(String userId, Product product1) {
        //getting the specified artist reference
        mDatabase = FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_CATEGORY).child(product1.catMKey).child(Constants.DATABASE_PATH_SUBCATEGORY).child(product1.subCatMKey).child(Constants.DATABASE_PATH_PRODUCTS).child(product1.getId()).child(CART_WISH_STATUS);
        //updating artist
        CartorWistStatus product = new CartorWistStatus();
        //Adding values
        product.setUserId(userId);
        product.setCartStatus("0");
        product.setWishlistStatus(product1.wishStatus);
        // CartorWistStatus product = new CartorWistStatus(userId, cartStatus, wishStatus);
        mDatabase.child(userId).setValue(product);

    }


    @Override
    protected void onRestart() {

       /* appController.cartCount(userid);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);*/
        invalidateOptionsMenu();
        super.onRestart();
    }

    @Override
    protected void onStart() {
       /* appController.cartCount(userid);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);*/
        invalidateOptionsMenu();
        super.onStart();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;


        }
        return super.onOptionsItemSelected(item);
    }

}
