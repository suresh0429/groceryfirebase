package com.suresh.firebasegrocery.User.Model;

/**
 * Created by pratap.kesaboyina on 01-12-2015.
 */
public class SingleItemModel {


    public String id;
    public String currentDate;
    public String imageType;
    public String imageUrl;
    public String category;
    public String subcategory;
    public String productName;
    public String productPrice;
    public String capacityWeight;
    public String availability;
    public String description;
    public String mKey;


    public SingleItemModel() {
    }

    public SingleItemModel(String id, String currentDate, String imageType, String imageUrl, String category, String subcategory, String productName, String productPrice, String capacityWeight, String availability, String description, String mKey) {
        this.id = id;
        this.currentDate = currentDate;
        this.imageType = imageType;
        this.imageUrl = imageUrl;
        this.category = category;
        this.subcategory = subcategory;
        this.productName = productName;
        this.productPrice = productPrice;
        this.capacityWeight = capacityWeight;
        this.availability = availability;
        this.description = description;
        this.mKey = mKey;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getCapacityWeight() {
        return capacityWeight;
    }

    public void setCapacityWeight(String capacityWeight) {
        this.capacityWeight = capacityWeight;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getmKey() {
        return mKey;
    }

    public void setmKey(String mKey) {
        this.mKey = mKey;
    }
}
